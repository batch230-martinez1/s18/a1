/*
	
	
	1.  [function WITHOUT return]
		Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function (test arguments: 5 and 15)
		-invoke and pass 2 arguments to the subtraction function (test arguments:  20 and 5)
*/

function checkSum(num1,num2){
    let sum = num1 + num2;
    console.log("Displayed sum of 5 and 15");
    console.log(sum);
}

checkSum(5, 15);

function checkDifference(num1, num2){
    let difference = num1 - num2;
    console.log("Displayed difference of 20 and 5");
    console.log(difference);
}

checkDifference(20, 5);


function checkProduct(num1, num2){
    let product = num1 * num2;
    console.log("The product of 50 and 10:");
    return product;
}
        
let returnProduct = checkProduct(50, 10);
console.log(returnProduct);


function checkQuotient(num1, num2){
    let quotient = num1 / num2;
    console.log("The Quotient of 50 and 10:");
    return quotient;
}
        
let returnQuotient = checkQuotient(50, 10);
console.log(returnQuotient);






function checkAreaOfaCircle(radius){
    let areaOfaCircle = 3.1416*radius**2;
    console.log("The result of getting area of a circle with 15 radius:");
    return areaOfaCircle;
    }
            
let circleArea = checkAreaOfaCircle(15);
console.log(circleArea);



function checkaverage(num1,num2,num3,num4){
    let average = (num1 + num2 + num3 + num4)/4;
    console.log("The average of 20, 40, 60, and 80:");
    return average;
    }
            
let averageVar = checkaverage(20, 40, 60, 80);
console.log(averageVar);


function checkIfPassed(score, total){
    let isPassed = ((score/total)*100) >= 75;
    console.log("Is 38/50 a passing score?");
    return isPassed; 

}
let isPassingScore =  checkIfPassed(38, 50);
console.log(isPassingScore);



